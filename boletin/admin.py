from django.contrib import admin
from .models import *
from .forms import RegModelForm
class AdminRegistrado(admin.ModelAdmin):
    list_display = ['nombre', 'email', 'fecha']
    form = RegModelForm
    list_display_links =  ['email']
    list_editable = ['nombre']
    list_filter = ['fecha']
    search_fields = ['nombre', 'email']
    #class Meta:
     #   model = Registrado



# Register your models here.
admin.site.register(Registrado, AdminRegistrado)
