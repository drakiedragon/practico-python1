from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from .models import Registrado
from .forms import RegModelForm
from .forms import ContactForm

# Create your views here.

def inicio(request):
    form = RegModelForm(request.POST or None)
    if request.user.is_authenticated():
        titulo = 'BIENVENIDO %s' % (request.user)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get('nombre')
        email2 = form_data.get('email')
        objeto = Registrado.objects.create(nombre=nombre2, email=email2)
        titulo = 'BIENVENIDOS A NUESTRA PRIMERA APLICACION WEB DJANGO 1.11'
    contexto = {
        'el_titulo': titulo,
        'el_formulario': form,
    }
    return render(request,'inicio.html',contexto)
def contacto(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        formulario_nombre = form.cleaned_data.get('nombre')
        formulario_email = form.cleaned_data.get('email')
        formulario_mensaje = form.cleaned_data.get('mensaje')
        asunto = 'Formulario de contacto web'
        email_from = settings.EMAIL_HOST_USER
        email_to = [email_from, 'roberto.moore@virginiogomez.cl']
        email_mensaje = 'Enviado por %s - Correo: %s - Mensaje: %s' %(formulario_nombre,formulario_email,formulario_mensaje)
        send_mail(asunto,
                  email_mensaje,
                  email_from,
                  email_to,
                  fail_silently=False)


       # print(form.cleaned_data)
        #nombre = form.cleaned_data.get('nombre')
        #email = form.cleaned_data.get('email')
        #mensaje = form.cleaned_data.get('mensaje')
        #print(nombre, email, mensaje)

        #for key in form.cleaned_data:
         #   print(key)
         #   print(form.cleaned_data.get(key))

        #for key, value in form.cleaned_data.items():
            #print(key,value)
    contexto = {
        "el_contacto": form,
    }
    return render(request, "contacto.html",contexto)

