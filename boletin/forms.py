from django.shortcuts import render
from django import forms
from .models import Registrado


class RegModelForm(forms.ModelForm):
    class Meta:
        model = Registrado
        campos = ['nombre', 'email']
        exclude =()

        def clean_mail(self):
            email = self.cleaned_data.get('email')
            email_base, proveedor = email.split('@')
            dominio, extension = proveedor.split('.')
            if not extension == 'edu':
                raise forms.ValidationError('Solo se aceptan correos con extension .EDU')
            return email

class ContactForm(forms.Form):
    nombre = forms.CharField(required=False)
    email = forms.EmailField()
    mensaje = forms.CharField(widget=forms.Textarea)